__iconsData: {
    
        'advant-1': {
            width: '48px',
            height: '48px'
        },
    
        'advant-2': {
            width: '48px',
            height: '48px'
        },
    
        'advant-3': {
            width: '34px',
            height: '34px'
        },
    
        'advant-4': {
            width: '46px',
            height: '46px'
        },
    
        'arr-down': {
            width: '9px',
            height: '5px'
        },
    
        'arr-right': {
            width: '5px',
            height: '9px'
        },
    
        'cart': {
            width: '22px',
            height: '22px'
        },
    
        'catalog-1': {
            width: '32px',
            height: '32px'
        },
    
        'catalog-10': {
            width: '34px',
            height: '34px'
        },
    
        'catalog-2': {
            width: '32px',
            height: '32px'
        },
    
        'catalog-3': {
            width: '32px',
            height: '32px'
        },
    
        'catalog-4': {
            width: '24px',
            height: '24px'
        },
    
        'catalog-5': {
            width: '32px',
            height: '32px'
        },
    
        'catalog-6': {
            width: '32px',
            height: '32px'
        },
    
        'catalog-7': {
            width: '32px',
            height: '32px'
        },
    
        'catalog-8': {
            width: '30px',
            height: '30px'
        },
    
        'catalog-9': {
            width: '28px',
            height: '28px'
        },
    
        'check': {
            width: '9px',
            height: '7px'
        },
    
        'city': {
            width: '48px',
            height: '48px'
        },
    
        'delivery': {
            width: '17px',
            height: '12px'
        },
    
        'edit': {
            width: '20px',
            height: '20px'
        },
    
        'facebook': {
            width: '20px',
            height: '20px'
        },
    
        'flag-usa': {
            width: '48px',
            height: '48px'
        },
    
        'globe': {
            width: '18px',
            height: '18px'
        },
    
        'heart-2': {
            width: '20.01px',
            height: '18px'
        },
    
        'heart-stroke-2': {
            width: '20.01px',
            height: '18px'
        },
    
        'heart-stroke': {
            width: '21px',
            height: '18px'
        },
    
        'heart': {
            width: '21px',
            height: '18px'
        },
    
        'instagram': {
            width: '20px',
            height: '20px'
        },
    
        'list': {
            width: '20px',
            height: '14px'
        },
    
        'map': {
            width: '16px',
            height: '20px'
        },
    
        'minus': {
            width: '8px',
            height: '2px'
        },
    
        'phone': {
            width: '19px',
            height: '20px'
        },
    
        'pinterest': {
            width: '20px',
            height: '20px'
        },
    
        'plus': {
            width: '8px',
            height: '8px'
        },
    
        'search': {
            width: '17px',
            height: '18px'
        },
    
        'star': {
            width: '16px',
            height: '16px'
        },
    
        'success': {
            width: '200px',
            height: '200px'
        },
    
        'twitter': {
            width: '20px',
            height: '20px'
        },
    
        'user': {
            width: '18px',
            height: '19px'
        },
    
        'youtube': {
            width: '20px',
            height: '20px'
        },
    
}