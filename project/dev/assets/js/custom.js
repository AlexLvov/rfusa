/*
| ----------------------------------------------------------------------------------
| TABLE OF CONTENT
| ----------------------------------------------------------------------------------
-Function for mobile
-Preloader
-Block Cards. Toggle list
-Search activate for mobile
-Switch payment
-Enumerator
-Modal Location activate
-Modal first activation submit
-Add card to favorite
-Customization scroll
-Mobile menu
-Activate for catalog
-Scale images
-Select customization
-Rating select
-Smooth navigation
-Sliders
-Slider numbers
-Mask input
-Slider background for last item
-Mobile filter
-Header goods on scroll
-FORM VALIDATION
*/



$(document).ready(function() {

    "use strict";


// Function for mobile
  var isMobile = /Android|webOS|iPhone|iPad|iPod|pocket|psp|kindle|avantgo|blazer|midori|Tablet|Palm|maemo|plucker|phone|BlackBerry|symbian|IEMobile|mobile|ZuneWP7|Windows Phone|Opera Mini/i.test(navigator.userAgent);



// Preloader

    var $preloader = $('#page-preloader'),
    $spinner   = $preloader.find('.spinner-loader');
    $spinner.fadeOut();
    $preloader.delay(50).fadeOut('slow');



// Block Cards. Toggle list.

  $(".b-card__list").each(function(index, el){
    if ($('.b-card__item', el).length > 5) {
      $(this).addClass('active');
      $(this).append('<span class="b-card__link b-card__link_primary">View More</span>');
    }
  });

  $(".b-card__list .b-card__link").on(isMobile ? 'touchend' : 'click', function() {
    $(this).parents('.b-card__list').toggleClass('active');
    $(this).text(function(i, text){
        return text === "View More" ? "View Less" : "View More";
    })
  });


// Search activate for mobile

  if ($(window).width() < 1200) {
    $('.header-search__submit').attr('type', 'button');
    $('.header-search__submit').on(isMobile ? 'touchend' : 'click', function() {
      $('.header-search').toggleClass('active');
    })
  }



// Switch payment

  $('.js-switch-payment').on(isMobile ? 'touchend' : 'click', function() {
    $('.b-order-payment__main').fadeToggle();
  })


// Enumerator

  $(".js-minus").on(isMobile ? 'touchend' : 'click', function() {
      var inputEl = jQuery(this).parent().children().next();
      var qty = inputEl.val();
      if (jQuery(this).parent().hasClass("js-minus"))
          qty++;
      else
          qty--;
      if (qty < 0)
          qty = 0;
      inputEl.val(qty);
  })


  $(".js-plus").on(isMobile ? 'touchend' : 'click', function() {
      var inputEl = jQuery(this).parent().children().next();
      var qty = inputEl.val();
      if (jQuery(this).hasClass("js-plus"))
          qty++;
      else
          qty--;
      if (qty < 0)
          qty = 0;
      inputEl.val(qty);
  })



// Modal Location

  var localValue = localStorage.getItem('location');

  if(localValue === null) {
    $(window).load(function() {
      $("#modalLocation").modal('show');
    })
  }

  $('.js-location').on(isMobile ? 'touchend' : 'click', function() {
    $('.js-location-btn').removeAttr('disabled');
    $('.js-input').slideDown();
  })

  $('.js-location-btn').on(isMobile ? 'touchend' : 'click', function() {
    localStorage.setItem('location', 'check');
  })




// Add card to favorite

  $('.js-favorite').on(isMobile ? 'touchend' : 'click', function() {
    $(this).toggleClass('active');
  });


// Customization scroll

  if ($(window).width() > 1199 && $('.js-scroll-pane').length) {
    $('.js-scroll-pane').jScrollPane();
  }


// Mobile menu

  $('.js-link-catalog').on(isMobile ? 'touchend' : 'click', function() {
    $('.header__link-mob-text').text(function(i, text){
        return text === "Catalog" ? "Main menu" : "Catalog";
    });
    $('.js-link-catalog .ic').toggleClass('active');
    $('.header__group-mobile, .header-catalog__wrap').fadeToggle();
    $('.header-catalog__item').removeClass('active');
  });

  if ($(window).width() < 1200) {
    $('.header-catalog__link').on(isMobile ? 'touchend' : 'click', function() {
      $('.js-link-catalog').hide();
      $('.js-close-catalog').show();
    })
    $('.js-close-catalog').on(isMobile ? 'touchend' : 'click', function() {
      $('.js-link-catalog').show();
      $('.js-close-catalog').hide();
      $('.header-catalog__item').removeClass('active');
    })
  }



// Activate for catalog
  if ($(window).width() > 1199) {
    $('.header-catalog__link').hover( function() {
      $('.js-scroll-pane').jScrollPane();
      $(this).next().addClass('active');
    })

     $.fn.widthSubmenu = function() {
        var widthCont = $('.container').width();
        $('.header-catalog__submenu').width(widthCont - widthCont * .25 + 30);
    }

    $(window).widthSubmenu();

    $(window).resize(function(){
      $(this).widthSubmenu();
    });
  }

  if ($(window).width() < 1200) {
    $('.header-catalog__link').on(isMobile ? 'touchend' : 'click', function() {
      $(this).parent().toggleClass('active');
    })
  }


// Scale images

  if ($('.img-scale').length) {
    $(function () { objectFitImages('.img-scale') });
  }


// Select customization

  if ($('.js-select').length) {
    $('.js-select').selectpicker();
  }


// Rating select
    $('.ui-rating__item').on(isMobile ? 'touchend' : 'click', function() {
      $(this).parent().children('.ui-rating__item').removeClass('active');
      $(this).prevAll('.ui-rating__item').addClass('active');
    })


// Smooth navigation

  $(".js-nav").on(isMobile ? 'touchend' : 'click', function (event) {
    event.preventDefault();
    var id  = $(this).attr('href'),
      top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 500);
  });


// Sliders

  if ($('.js-slider').length) {
    $('.js-slider').slick();
  }


// Slider numbers

  if ($('#filterPrice').length) {

    var keypressSlider = document.getElementById('filterPrice');
      var input0 = document.getElementById('input-with-keypress-0');
      var input1 = document.getElementById('input-with-keypress-1');
      var inputs = [input0, input1];

      noUiSlider.create(keypressSlider, {
          start: [10, 9000],
          connect: true,
          step: 10,
          format: wNumb({
            decimals: 0,
            prefix: '$'
          }),
          range: {
              'min': 0,
              'max': 9999
          }
      });

      keypressSlider.noUiSlider.on('update', function (values, handle) {
          inputs[handle].value = values[handle];
      });
  }


// Mask input

  if ($('input[type=tel]').length) {
    $("input[type=tel]").mask("+7 (999) 999 99 99");
  };


// Slider background for last item

  if ($('.b-goods-slider').length) {

    $('.b-goods-slider').append('<div class="b-goods-slider__op"></div>');

    function bgForSlider() {
      var goodsWidth = $('.b-goods-slider .b-goods').outerWidth(),
          goodsHeight = $('.b-goods-slider .b-goods').outerHeight(),
          sliderWidth = $('.b-goods-slider').width() % goodsWidth;

      $('.b-goods-slider__op').css({'width': sliderWidth, 'height': goodsHeight});
    }

    $(document).ready(bgForSlider);
    $(window).resize(bgForSlider);
  }


// Mobile filter

  if ($(window).width() < 1200 && $('.b-filter').length) {
    $('#modalFilter').addClass('ui-modal modal modal_full fade');
    $('#modalFilter').children().addClass('modal-dialog');
    $('#modalFilter .modal-dialog').children().addClass('modal-content');
  }

  $(window).resize(function(){
    if ($(window).width() < 1200 && $('#modalFilter').not('.ui-modal')) {
      $('#modalFilter').addClass('ui-modal modal modal_full fade');
      $('#modalFilter').children().addClass('modal-dialog');
      $('#modalFilter .modal-dialog').children().addClass('modal-content');
    }

    if ($(window).width() > 1199 && $('#modalFilter').is('.ui-modal')) {
      $('#modalFilter').removeClass('ui-modal modal modal_full fade').show();
      $('#modalFilter').children().removeClass('modal-dialog');
      $('#modalFilter').find('.modal-content').removeClass('modal-content');
    }
  });


// Header goods on scroll

  if ($('.b-goods-header').length && $(window).width() > 992) {
      $(window).scroll(function () {
        if ($(this).scrollTop() > 234) {
            $('.b-goods-header').slideDown();
        } else {
            $('.b-goods-header').slideUp();
        }
    });
  }




});


// FORM VALIDATION

window.addEventListener('load', function() {
  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.getElementsByClassName('needs-validation');
  // Loop over them and prevent submission
  var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  });
}, false);

